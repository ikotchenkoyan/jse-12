package ru.t1.kotchenko.tm.repository;

import ru.t1.kotchenko.tm.api.repository.IProjectRepository;
import ru.t1.kotchenko.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kotchenko.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    public ProjectRepository() {
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        final List<Project> result = new ArrayList<>(projects);
        result.sort(comparator);
        return result;
    }

    @Override
    public Project add(final Project project) {
        projects.add(project);
        return project;
    }

    @Override
    public void deleteAll() {
        projects.clear();
    }

    @Override
    public Project findOneById(final String id) {
        for (final Project project : projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        return projects.get(index);
    }

    @Override
    public Project remove(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeById(final String id) {
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        return remove(project);
    }

    @Override
    public Project removeByIndex(final Integer index) {
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        return remove(project);
    }

    @Override
    public boolean existById(final String id) {
        return findOneById(id) != null;
    }

    public int getSize() {
        return projects.size();
    }
}

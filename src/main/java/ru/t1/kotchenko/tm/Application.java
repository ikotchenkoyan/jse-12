package ru.t1.kotchenko.tm;

import ru.t1.kotchenko.tm.api.component.IBootstrap;
import ru.t1.kotchenko.tm.component.Bootstrap;

public final class Application {

    public static void main(final String[] args) {
        final IBootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
package ru.t1.kotchenko.tm.api.service;

import ru.t1.kotchenko.tm.model.Task;

public interface IProjectTaskService {

    Task bindTaskToProject(String projectId, String taskId);

    void removeProjectById(String projectId);

    void removeProjects();

    Task unbindTaskToProject(String projectId, String taskId);

}

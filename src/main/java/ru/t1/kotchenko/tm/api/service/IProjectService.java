package ru.t1.kotchenko.tm.api.service;

import ru.t1.kotchenko.tm.api.repository.IProjectRepository;
import ru.t1.kotchenko.tm.enumerated.Sort;
import ru.t1.kotchenko.tm.enumerated.Status;
import ru.t1.kotchenko.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IProjectRepository {

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    List<Project> findAll(Sort sort);

    Project create(String name);

    Project create(String name, String description);

    Project create(String name, Status status);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

}

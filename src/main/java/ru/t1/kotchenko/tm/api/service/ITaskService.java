package ru.t1.kotchenko.tm.api.service;

import ru.t1.kotchenko.tm.api.repository.ITaskRepository;
import ru.t1.kotchenko.tm.enumerated.Sort;
import ru.t1.kotchenko.tm.enumerated.Status;
import ru.t1.kotchenko.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends ITaskRepository {

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    List<Task> findAll(Sort sort);

    Task create(String name);

    Task create(String name, String description);

    Task create(String name, Status status);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

}

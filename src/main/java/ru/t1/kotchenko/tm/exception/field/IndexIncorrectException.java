package ru.t1.kotchenko.tm.exception.field;

public final class IndexIncorrectException extends AbstractFieldExceotion {

    public IndexIncorrectException() {
        super("Error! Index is incorrect.");
    }

}

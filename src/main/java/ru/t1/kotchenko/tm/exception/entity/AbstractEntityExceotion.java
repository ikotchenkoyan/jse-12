package ru.t1.kotchenko.tm.exception.entity;

import ru.t1.kotchenko.tm.exception.AbstractException;

public abstract class AbstractEntityExceotion extends AbstractException {

    public AbstractEntityExceotion() {
    }

    public AbstractEntityExceotion(String message) {
        super(message);
    }

    public AbstractEntityExceotion(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractEntityExceotion(Throwable cause) {
        super(cause);
    }

    public AbstractEntityExceotion(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

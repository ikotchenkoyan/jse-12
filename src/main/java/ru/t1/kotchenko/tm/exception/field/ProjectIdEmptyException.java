package ru.t1.kotchenko.tm.exception.field;

public final class ProjectIdEmptyException extends AbstractFieldExceotion {

    public ProjectIdEmptyException() {
        super("Error! Project id is empty.");
    }

}

package ru.t1.kotchenko.tm.exception.field;

public final class NameEmptyException extends AbstractFieldExceotion {

    public NameEmptyException() {
        super("Error! Name is empty.");
    }

}

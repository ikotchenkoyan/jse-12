package ru.t1.kotchenko.tm.exception.system;

public final class ArgumentNotSupportedException extends AbstractSystemExceotion {

    public ArgumentNotSupportedException() {
        super("Error! Argument not supported.");
    }

    public ArgumentNotSupportedException(final String argument) {
        super("Error! Argument ''" + argument + "'' not supported.");
    }

}

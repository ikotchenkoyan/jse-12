package ru.t1.kotchenko.tm.exception.field;

public final class TaskIdEmptyException extends AbstractFieldExceotion {

    public TaskIdEmptyException() {
        super("Error! Task id is empty.");
    }

}

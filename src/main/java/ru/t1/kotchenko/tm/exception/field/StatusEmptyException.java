package ru.t1.kotchenko.tm.exception.field;

public final class StatusEmptyException extends AbstractFieldExceotion {

    public StatusEmptyException() {
        super("Error! Status is empty.");
    }

}

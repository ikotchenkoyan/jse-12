package ru.t1.kotchenko.tm.exception.field;

import ru.t1.kotchenko.tm.exception.AbstractException;

public abstract class AbstractFieldExceotion extends AbstractException {

    public AbstractFieldExceotion() {
    }

    public AbstractFieldExceotion(String message) {
        super(message);
    }

    public AbstractFieldExceotion(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractFieldExceotion(Throwable cause) {
        super(cause);
    }

    public AbstractFieldExceotion(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

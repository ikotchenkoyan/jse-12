package ru.t1.kotchenko.tm.exception.system;

import ru.t1.kotchenko.tm.exception.AbstractException;

public abstract class AbstractSystemExceotion extends AbstractException {

    public AbstractSystemExceotion() {
    }

    public AbstractSystemExceotion(String message) {
        super(message);
    }

    public AbstractSystemExceotion(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractSystemExceotion(Throwable cause) {
        super(cause);
    }

    public AbstractSystemExceotion(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

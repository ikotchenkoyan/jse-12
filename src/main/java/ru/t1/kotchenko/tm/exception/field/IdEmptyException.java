package ru.t1.kotchenko.tm.exception.field;

public final class IdEmptyException extends AbstractFieldExceotion {

    public IdEmptyException() {
        super("Error! Id is empty.");
    }

}
